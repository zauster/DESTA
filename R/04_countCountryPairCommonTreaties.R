library(data.table)
library(magrittr)
library(stringr)
library(countrycode)
library(here)
setwd(here::here())

## load data
load("data/00_config/Configuration.RData")
load("data/02_tmp/02_DESTA_depth.RData")

## EU Korea Agreement
## this_number <- "815"
## this_number <- "899"


## get all treaty numbers
all_treaty_numbers <- depthmeta[, unique(number)]

desta_dims <- c("full_fta", "standards", "investments",
                "services", "procurement", "competition", "iprs",
                "depth_index")
ct_dims <- paste0("CT_", c("full_fta", "standards", "investments",
                "services", "procurement", "competition", "iprs",
                "depth"))


grid_list <- list()
for(this_number in all_treaty_numbers) {
  ## message(this_number, ", ", appendLF = FALSE)

  tmp <- depthmeta[number == this_number, ]
  treaty_countries <- sort(union(tmp[, unique(country1.iso3)],
                                 tmp[, unique(country2.iso3)]))
  treaty_entryyear <- tmp[!is.na(entryforceyear), unique(entryforceyear)]
  if(length(treaty_entryyear) == 0) {
    treaty_entryyear <- NA
  }

  ## treaty_depth <- tmp[, max(unique(depth_index))]
  treaty_desta <- tmp[, lapply(.SD, function(x) max(unique(x))),
                      .SDcols = desta_dims]

  ## treaty_name <- tmp[!is.na(name), unique(name)]
  ## if(length(treaty_name) > 1) {
  ##   stop("More than one name!")
  ## }

  if(length(treaty_entryyear) > 1) {
    warning(paste0(this_number, ": More than one entryyear!"))
    treaty_entryyear <- min(treaty_entryyear)
  }

  tmp_grid <- expand.grid(c1 = treaty_countries,
                          c2 = treaty_countries,
                          entryforceyear = treaty_entryyear,
                          number = this_number,
                          ## name = treaty_name,
                          ## depth = treaty_depth,
                          count = 1,
                          KEEP.OUT.ATTRS = FALSE,
                          stringsAsFactors = FALSE)
  setDT(tmp_grid)
  tmp_grid <- tmp_grid[c1 != c2, ]
  tmp_grid <- cbind(tmp_grid, treaty_desta)

  ## tmp_orig <- tmp[, .(country1.iso3, country2.iso3)] %>%
  ##   setnames(c("c1", "c2")) %>%
  ##   .[, remove := TRUE]
  ## setkey(tmp_orig, c1, c2)
  ## setkey(tmp_grid, c1, c2)
  ## tmp_grid <- tmp_orig[tmp_grid]
  ## tmp_grid <- tmp_grid[is.na(remove), ] %>%
  ##   .[, remove := NULL]

  grid_list[[this_number]] <- tmp_grid
}
## message("")

griddt <- rbindlist(grid_list)

## ## test
## griddt[c1 == "AUT" & c2 == "DEU" & entryforceyear == 2011, ]
## xxx_numbers <- griddt[c1 == "AUT" & c2 == "DEU" &
##                       entryforceyear == 1995,
##                       unique(number)]
## depthmeta[number %chin% xxx_numbers,
##           .(entry = unique(entryforceyear),
##             sign = unique(year)),
##           by = .(name)]
griddt[c1 == "AUT" & c2 == "CHE", ]

## aggregate to countrypair-years
CommonTreaties <- griddt[, .(CT = sum(count),
                             CT_depth = max(depth_index),
                             CT_full_fta = max(full_fta),
                             CT_standards = max(standards),
                             CT_investments = max(investments),
                             CT_services = max(services),
                             CT_procurement = max(procurement),
                             CT_competition = max(competition),
                             CT_iprs = max(iprs)),
                         keyby = .(c1, c2, entryforceyear)]


min_year <- CommonTreaties[, min(entryforceyear, na.rm = TRUE)]
max_year <- CommonTreaties[, max(entryforceyear, na.rm = TRUE)]
all_countries <- union(CommonTreaties[, unique(c1)],
                       CommonTreaties[, unique(c2)])
full_grid <- expand.grid(c1 = all_countries,
                         c2 = all_countries,
                         entryforceyear = min_year:max_year,
                         KEEP.OUT.ATTRS = FALSE,
                         stringsAsFactors = FALSE)
setDT(full_grid)
full_grid <- full_grid[c1 != c2, ]

setkey(full_grid, c1, c2, entryforceyear)
setkey(CommonTreaties, c1, c2, entryforceyear)
CommonTreaties <- merge(CommonTreaties, full_grid,
                        all.y = TRUE)
setorder(CommonTreaties, c1, c2, entryforceyear)

CommonTreaties[is.na(CT), CT := 0]
for(col in ct_dims) {
  CommonTreaties[is.na(get(col)), (col) := 0]
}

CommonTreaties[, `:=`(cumCT = cumsum(CT),
                      cumCT_depth = cumsum(CT_depth),
                      cumCT_full_fta = cumsum(CT_full_fta),
                      cumCT_standards = cumsum(CT_standards),
                      cumCT_investments = cumsum(CT_investments),
                      cumCT_services = cumsum(CT_services),
                      cumCT_procurement = cumsum(CT_procurement),
                      cumCT_competition = cumsum(CT_competition),
                      cumCT_iprs = cumsum(CT_iprs)),
               by = .(c1, c2)]

CommonTreaties[c1 == "AUT" & c2 == "DEU", ]
## CommonTreaties[c1 == "AUT" & c2 == "ISR", ]
## CommonTreaties[c1 == "USA" & c2 == "MEX", ]
## CommonTreaties[c1 == "CHN" & c2 == "AUS", ]
## CommonTreaties[c1 == "NZL" & c2 == "AUS", ]
## CommonTreaties[c1 == "AUS" & c2 == "NZL", ]
## CommonTreaties[c1 == "AUT" & c2 == "CHE", ]


## save
save(CommonTreaties, file = "data/output/CommonTreaties_byCountryPair.RData")
