all: data/output/DESTA_byAgreement_EntryForceYear_DepthPlus1.RData data/output/DESTA_byAgreement_EntryForceYear.RData ./R/05_showCountryTreaties.R ./R/05_testData.R data/output/CalcCentralities_EUas1_p1.RData data/output/CalcCentralities_EUas1.RData data/output/CalcCentralities_p1.RData data/output/CalcCentralities.RData ./R/20_visualizeCentralities.R ./R/20_visualizeCountryPairCommonTreaties.R
data/00_config/Configuration.RData: ./R/00_config.R
	Rscript ./R/00_config.R

data/02_tmp/Depth_raw.RData: ./R/01_readin_Depth.R
	Rscript ./R/01_readin_Depth.R

data/02_tmp/Meta_raw.RData: ./R/01_readin_MetaData.R
	Rscript ./R/01_readin_MetaData.R

data/02_tmp/Withdrawals_raw.RData: ./R/01_readin_Withdrawals.R
	Rscript ./R/01_readin_Withdrawals.R

data/02_tmp/01_DepthMeta_raw.RData: ./R/02_mergeData.R
	Rscript ./R/02_mergeData.R

data/02_tmp/02_DESTA_depth.RData: ./R/03_doAdjustments.R
	Rscript ./R/03_doAdjustments.R

data/output/DESTA_depth_byAgreement_EntryForceYear.rds data/output/DESTA_depth_byAgreement_SigningYear.rds data/output/DESTA_depth_max_EntryForceYear.rds data/output/DESTA_depth_max_SigningYear.rds: ./R/04_convertToPanel_byDepth.R
	Rscript ./R/04_convertToPanel_byDepth.R

data/output/DESTA_byAgreement_EntryForceYear.RData data/output/DESTA_byAgreement_EntryForceYear_DepthPlus1.RData: ./R/04_convertToPanel_byTreaty.R
	Rscript ./R/04_convertToPanel_byTreaty.R

data/output/CommonTreaties_byCountryPair.RData: ./R/04_countCountryPairCommonTreaties.R
	Rscript ./R/04_countCountryPairCommonTreaties.R

data/AllCountries.rds data/FullEdgeList.RData: ./R/10_convertDESTA_toGraph.R
	Rscript ./R/10_convertDESTA_toGraph.R

data/Depth2Distance_Mapping.RData: ./R/10_createDepth2Distance_Mapping.R
	Rscript ./R/10_createDepth2Distance_Mapping.R

data/NodeProperties.RData: ./R/10_getNodeDescriptions.R
	Rscript ./R/10_getNodeDescriptions.R

data/output/CalcCentralities.RData data/output/CalcCentralities_EUas1.RData data/output/CalcCentralities_EUas1_p1.RData data/output/CalcCentralities_list.RData data/output/CalcCentralities_p1.RData: ./R/11_calculateCentralities.R
	Rscript ./R/11_calculateCentralities.R

./R/04_convertToPanel_byDepth.R ./R/04_convertToPanel_byTreaty.R ./R/04_countCountryPairCommonTreaties.R: data/00_config/Configuration.RData data/02_tmp/02_DESTA_depth.RData
	--touch ./R/04_convertToPanel_byDepth.R ./R/04_convertToPanel_byTreaty.R ./R/04_countCountryPairCommonTreaties.R

./R/01_readin_Depth.R: data/01_raw/depth_version_01_05.csv data/00_config/Configuration.RData
	--touch ./R/01_readin_Depth.R

./R/01_readin_MetaData.R: data/01_raw/list_of_treaties_01_05_dyads.csv data/00_config/Configuration.RData
	--touch ./R/01_readin_MetaData.R

./R/01_readin_Withdrawals.R: data/01_raw/list_of_withdrawals_dyads.csv data/00_config/Configuration.RData
	--touch ./R/01_readin_Withdrawals.R

./R/03_doAdjustments.R: data/02_tmp/01_DepthMeta_raw.RData data/00_config/Configuration.RData
	--touch ./R/03_doAdjustments.R

./R/05_testData.R: data/02_tmp/02_DESTA_depth.RData data/output/DESTA_depth_byAgreement_SigningYear.rds data/output/DESTA_depth_max_SigningYear.rds data/output/DESTA_depth_byAgreement_EntryForceYear.rds data/output/DESTA_depth_max_EntryForceYear.rds
	--touch ./R/05_testData.R

./R/05_showCountryTreaties.R: data/02_tmp/Meta_raw.RData data/02_tmp/Depth_raw.RData data/02_tmp/01_DepthMeta_raw.RData data/output/DESTA_depth_byAgreement_EntryForceYear.rds data/output/DESTA_depth_max_EntryForceYear.rds
	--touch ./R/05_showCountryTreaties.R

./R/02_mergeData.R: data/02_tmp/Meta_raw.RData data/02_tmp/Withdrawals_raw.RData data/02_tmp/Depth_raw.RData data/00_config/Configuration.RData
	--touch ./R/02_mergeData.R

./R/10_getNodeDescriptions.R: data/AllCountries.rds
	--touch ./R/10_getNodeDescriptions.R

./R/10_convertDESTA_toGraph.R: data/Depth2Distance_Mapping.RData data/EUMembership.RData data/output/DESTA_depth_max_EntryForceYear.rds
	--touch ./R/10_convertDESTA_toGraph.R

./R/11_calculateCentralities.R: data/EUMembership.RData data/FullEdgeList.RData data/NodeProperties.RData data/EUMembership.RData
	--touch ./R/11_calculateCentralities.R

./R/20_visualizeCentralities.R: data/output/CalcCentralities_list.RData
	--touch ./R/20_visualizeCentralities.R

./R/20_visualizeCountryPairCommonTreaties.R: data/output/CommonTreaties_byCountryPair.RData
	--touch ./R/20_visualizeCountryPairCommonTreaties.R

